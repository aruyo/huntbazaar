# README

## Tech stack

1. Laravel 8.x
2. Tailwind CSS
3. PHP >7.1
4. Artisan test runner

## How to install

1. composer install
2. php artisan migrate
3. php artisan db:seed
4. php artisan queue:work

# How to install

```php
composer install
```

## How to run unit testing


```php
php artisan test
```

**Expected ouput**


```
PASS  Tests\Unit\ExampleTest
  ✓ basic test

   PASS  Tests\Feature\DatabaseTest
  ✓ example

   PASS  Tests\Feature\ExampleTest
  ✓ basic test

   PASS  Tests\Feature\GuestTest
  ✓ example

   PASS  Tests\Feature\RouteMiddlewareTest
  ✓ example

   PASS  Tests\Feature\RouteTest
  ✓ example

  Tests:  6 passed
  Time:   11.50s
```
