<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Guest;
use App\Mail\InviteGuest;
use App\Models\GuestToken;
use ReallySimpleJWT\Token;
use Illuminate\Http\Request;
use App\Models\DesignerFavorite;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\InviteRequest;
use Illuminate\Support\Facades\Session;

class GuestController extends Controller
{
    public function index()
    {
        $data = Guest::with('tokens')->get();
        return view('dashboard.guest.index')->with([
            'data' => $data
        ]);
    }

    public function create()
    {
        return view('dashboard.guest.create');
    }

    public function post(InviteRequest $request)
    {

        $valGuest = Guest::where('email', $request->get('email'))->first();
        if ($valGuest) {
            $request->session()->flash('valEmail', 'An error just occurred with following message:');
            $request->session()->flash('valEmail2','An email address you enter already on the Guest List');
            return redirect('/guest/list');
        }

        $userId = 99;
        $secret = 'sec!ReT423*&';
        $expiration = time() * 3600;
        $issuer = 'HuntBazzar';

        $token = Token::create($userId, $secret, $expiration, $issuer);

        $guestToken = new GuestToken;
        $guestToken->token_invitation = $token;
        $guestToken->save();

        $guest = new Guest;
        $guest->id_token = $guestToken->id;
        $guest->email = $request->get('email');
        $guest->save();

        $arr = [
            'token' => $guestToken->token_invitation,
            'email' => $guest->email
        ];

        try {
        Mail::to($request->get('email'))->send(new InviteGuest($arr));
        $request->session()->flash('addGuest', 'The data has been successful update');
        return redirect('/guest/list');
        } catch (Exception $e) {
            return response (['status' => false,'errors' => $e->getMessage()]);
        }
    }

    public function destroy(Request $request,$id)
    {
        $data = Guest::findOrFail($id);
        $data->delete();

        $request->session()->flash('deleteGuest', 'The data has been successful deleted');
        return redirect('/guest/list');
    }

    public function detail($id)
    {
        $data = Guest::with('designers', 'tokens')->where('id', $id)->get();
        return view('dashboard.guest.detail')->with([
            'data' => $data
        ]);
    }
}
