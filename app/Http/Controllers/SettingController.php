<?php

namespace App\Http\Controllers;

use App\Models\Guest;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Models\DesignerFavorite;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    public function triggerTime()
    {
        $data = Setting::all()->first();

        return response()->json($data);
    }

    public function designerList()
    {
        $path = public_path('data/designer.json');
        $json = json_decode(file_get_contents($path), true);
        return response()->json($json);
    }

    public function dashboard()
    {

        $data = Guest::with('tokens')->get();

        $arrDesign = DesignerFavorite::groupBy('name_designer')->select('name_designer', DB::raw('count(*) as total'))->get();

        return view('dashboard.index')->with([
            'data' => $data,
            'designer' => $arrDesign
        ]);
    }

    public function changeTime(Request $request)
    {
        $data = Setting::all()->first();

        $arr = explode(' ', $data->datetime);
        $clock = $arr[0].'T'.$arr[1];
        if ($clock == $request->get('datetime') ) {
            $request->session()->flash('notChange', 'the time you entered is still the same');
            return redirect('/setting');
        } else {
            $data->datetime = $request->get('datetime');
            $data->save();

            $request->session()->flash('update', 'The data has been successful update');
            return redirect('/setting');
        }

    }

    public function setting()
    {
        $data = Setting::all()->first();
        return view('dashboard.setting')->with([
            'data' => $data
        ]);
    }
}
