<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Guest;
use App\Models\GuestToken;
use Illuminate\Http\Request;

class GuestValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $email = $request->query('email');
        $token = $request->query('token');

        $emailGuest = Guest::where('email', $email)->first();
        $tokenGuest = GuestToken::where('token_invitation', $token)->first();

        if (!$emailGuest || !$email || !$tokenGuest || !$token) {
            return redirect('/login');
        } else {
            if ($tokenGuest->register_code) {
                return redirect("/registeration/$tokenGuest->register_code");
            }
            return $next($request);
        }
    }
}
