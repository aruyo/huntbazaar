<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DesignerFavorite extends Model
{
    use HasFactory;

    protected $table = 'guest_favorite_designer';

    public function guest()
    {
        return $this->hasOne(Guest::class, 'id', 'id_guest');
    }
}
