<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    use HasFactory;

    public function tokens()
    {
        return $this->hasOne(GuestToken::class, 'id');
    }

    public function designers()
    {
        return $this->belongsToMany(DesignerFavorite::class, 'guests', 'id', 'id', 'id', 'id_guest');
    }
}
