@extends('layouts.auth-layout')
@section('content')
    <main>
        <section class="absolute w-full h-full">
            <div class="absolute top-0 w-full h-full bg-blue-700 bg-register-2">
            </div>
            <div class="container mx-auto px-4 h-full">
                <div class="flex content-center items-center justify-center h-full">
                    <div class="w-full lg:w-4/12 px-4">
                        @if ($errors->any())
                        <div class="w-full mb-8">
                            <div class="relative flex flex-col min-w-0 mx-auto break-words bg-white w-full shadow-lg rounded">
                                <div class="flex w-full mx-auto bg-notice-fail">
                                    <div class="w-16 bg-green">
                                        <div class="p-4 py-8">
                                            <svg class="my-auto h-11 w-11 text-white fill-current" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 512 512">
                                                <path
                                                    d="M437.019 74.981C388.667 26.629 324.38 0 256 0S123.333 26.63 74.981 74.981 0 187.62 0 256s26.629 132.667 74.981 181.019C123.332 485.371 187.62 512 256 512s132.667-26.629 181.019-74.981C485.371 388.667 512 324.38 512 256s-26.629-132.668-74.981-181.019zM256 470.636C137.65 470.636 41.364 374.35 41.364 256S137.65 41.364 256 41.364 470.636 137.65 470.636 256 374.35 470.636 256 470.636z" />
                                                <path
                                                    d="M256 235.318c-11.422 0-20.682 9.26-20.682 20.682v94.127c0 11.423 9.26 20.682 20.682 20.682 11.423 0 20.682-9.259 20.682-20.682V256c0-11.422-9.259-20.682-20.682-20.682zM270.625 147.248A20.826 20.826 0 0 0 256 141.19a20.826 20.826 0 0 0-14.625 6.058 20.824 20.824 0 0 0-6.058 14.625 20.826 20.826 0 0 0 6.058 14.625A20.83 20.83 0 0 0 256 182.556a20.826 20.826 0 0 0 14.625-6.058 20.826 20.826 0 0 0 6.058-14.625 20.839 20.839 0 0 0-6.058-14.625z" />
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="w-auto text-white items-center py-4 px-4">
                                        <span class="text-lg font-bold pb-4">
                                            Notice
                                        </span>
                                        <p class="leading-tight mt-4">
                                            We can't find a user with that email address.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if (session('status'))
                            <div class="w-full mb-8">
                            <div class="relative flex flex-col min-w-0 mx-auto break-words bg-white w-full shadow-lg rounded">
                                <div class="flex w-full mx-auto bg-notice-success">
                                    <div class="w-16 bg-green">
                                        <div class="p-4 py-8">
                                            <svg class="my-auto h-11 w-11 text-white fill-current" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 512 512">
                                                <path
                                                    d="M437.019 74.981C388.667 26.629 324.38 0 256 0S123.333 26.63 74.981 74.981 0 187.62 0 256s26.629 132.667 74.981 181.019C123.332 485.371 187.62 512 256 512s132.667-26.629 181.019-74.981C485.371 388.667 512 324.38 512 256s-26.629-132.668-74.981-181.019zM256 470.636C137.65 470.636 41.364 374.35 41.364 256S137.65 41.364 256 41.364 470.636 137.65 470.636 256 374.35 470.636 256 470.636z" />
                                                <path
                                                    d="M256 235.318c-11.422 0-20.682 9.26-20.682 20.682v94.127c0 11.423 9.26 20.682 20.682 20.682 11.423 0 20.682-9.259 20.682-20.682V256c0-11.422-9.259-20.682-20.682-20.682zM270.625 147.248A20.826 20.826 0 0 0 256 141.19a20.826 20.826 0 0 0-14.625 6.058 20.824 20.824 0 0 0-6.058 14.625 20.826 20.826 0 0 0 6.058 14.625A20.83 20.83 0 0 0 256 182.556a20.826 20.826 0 0 0 14.625-6.058 20.826 20.826 0 0 0 6.058-14.625 20.839 20.839 0 0 0-6.058-14.625z" />
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="w-auto text-white items-center py-4 px-4">
                                        <span class="text-lg font-bold pb-4">
                                            Notice
                                        </span>
                                        <p class="leading-tight mt-4">
                                            {{ session('status') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div
                            class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300 border-0">
                            <div class="rounded-t mb-0 px-6 py-6">
                                <div class="text-center mb-3">
                                    <h6 class="text-gray-600 text-sm font-bold">
                                        Reset Password Account
                                    </h6>
                                </div>
                                <hr class="mt-6 border-b-1 border-gray-400" />
                            </div>
                            {{-- <x-jet-validation-errors class="my-4 mx-4 text-sm " /> --}}
                            <div class="flex-auto px-4 lg:px-10 py-10 pt-0">
                                <form method="POST" action="{{ route('password.email') }}">
                                    @csrf
                                    <div class="relative w-full mb-3">
                                        <label class="block uppercase text-gray-700 text-xs font-bold mb-2"
                                            for="grid-password">Email</label><input type="email"
                                            class="px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full transition-custom"
                                            placeholder="Email"" name="email"
                                            value="{{ old('email') }}" required autofocus />
                                    </div>
                                    <div class="text-center mt-6">
                                        <button type="submit"
                                            class="bg-blue-700 text-white active:bg-blue-900 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full transition-custom"
                                            type="button"">
                                            Submit
                                        </button>
                                    </div>
                                </form>
                                <div class="text-center mt-4">
                                    <a class="hover:text-blue-800" href="/login">Back to Login ?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="absolute w-full bottom-0 bg-blue-700 pb-6">
                <div class="container mx-auto px-4">
                    <hr class="mb-6 border-b-1 border-blue-800" />
                    <div class="flex flex-wrap items-center md:justify-between justify-center">
                        <div class="w-full text-center px-4">
                            <div class="text-sm text-white font-semibold py-1">
                                Copyright © 2020
                                <a href="#" class="text-white hover:text-gray-400 text-sm font-semibold py-1">Megalodon</a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </section>
    </main>
@endsection
