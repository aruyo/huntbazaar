@extends('layouts.auth-layout')
@section('content')
    <main>
        <section class="absolute w-full h-full">
            <div class="absolute top-0 w-full h-full bg-blue-700 bg-register-2">
            </div>
            <div class="container mx-auto px-4 h-full">
                <div class="flex content-center items-center justify-center h-full">
                    <div class="w-full lg:w-4/12 px-4">
                        <div
                            class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300 border-0">
                            <div class="rounded-t mb-0 px-6 py-6">
                                <div class="text-center mb-3">
                                    <h6 class="text-gray-600 text-sm font-bold">
                                        Welcome
                                    </h6>
                                </div>
                                <hr class="mt-6 border-b-1 border-gray-400" />
                            </div>
                            <x-jet-validation-errors class="my-4 mx-4 text-sm " />
                            <div class="flex-auto px-4 lg:px-10 py-10 pt-0">
                                @if (session('status'))
                                    <div class="mb-4 font-medium text-sm text-green-600">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <form method="POST" action="{{ route('password.update') }}">
                                    @csrf
                                    <input type="hidden" name="token" value="{{ $request->route('token') }}">
                                    <div class="relative w-full mb-3">
                                        <label class="block uppercase text-gray-700 text-xs font-bold mb-2"
                                            for="grid-password">Email</label><input type="email"
                                            class="px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full transition-custom"
                                            placeholder="Email" name="email"
                                            value="{{ old('email') }}" required autofocus />
                                    </div>
                                    <div class="relative w-full mb-3">
                                        <label class="block uppercase text-gray-700 text-xs font-bold mb-2"
                                            for="grid-password">Password</label><input type="password"
                                            class="px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full transition-custom"
                                            placeholder="Password" name="password"
                                            value="{{ old('password') }}" />
                                    </div>
                                    <div class="relative w-full mb-3">
                                        <label class="block uppercase text-gray-700 text-xs font-bold mb-2"
                                            for="grid-password">Password</label><input type="password"
                                            class="px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full transition-custom"
                                            placeholder="Password Confirmation" name="password_confirmation"
                                            value="{{ old('password_confirmation') }}" />
                                    </div>
                                    <div class="text-center mt-6">
                                        <button type="submit"
                                            class="bg-blue-700 text-white active:bg-blue-900 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full transition-custom"
                                            type="button">
                                            Reset Password
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="absolute w-full bottom-0 bg-blue-700 pb-6">
                <div class="container mx-auto px-4">
                    <hr class="mb-6 border-b-1 border-blue-800" />
                    <div class="flex flex-wrap items-center md:justify-between justify-center">
                        <div class="w-full text-center px-4">
                            <div class="text-sm text-white font-semibold py-1">
                                Copyright © 2020
                                <a href="#" class="text-white hover:text-gray-400 text-sm font-semibold py-1">Megalodon</a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </section>
    </main>
@endsection
