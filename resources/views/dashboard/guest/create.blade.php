@extends('layouts.global')
@section('content')
    <div class="flex flex-wrap mt-4">
        <div class="w-full xl:w-8/12 mb-12 xl:mb-0 px-4 mx-auto">
            <div class="relative flex flex-col min-w-0 break-words bg-white w-full mx-auto mb-6 shadow-lg rounded">
                <div class="rounded-t mb-0 px-4 py-3 border-0">
                    <div class="flex flex-wrap items-center">
                        <div class="relative w-full px-4 max-w-full flex-grow flex-1">
                            <h3 class="font-semibold text-base text-gray-800">
                                Invitate Guest
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="block w-full overflow-x-auto">
                    <!-- Projects table -->
                    <form class="mt-6 mx-6" method="POST" action="/guest/invite">
                        @csrf
                        <label for="email" class="block mt-2 text-xs font-semibold text-gray-600 uppercase">E-mail</label>
                        <input id="email" type="email" name="email" placeholder="user@email.com" autocomplete="email"
                            class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner"
                            required />
                        <button type="submit"
                            class="w-full py-3 mb-6 mt-6 font-medium tracking-widest text-white uppercase bg-blue-800 shadow-lg focus:outline-none hover:bg-blue-900 hover:shadow-none">
                            Invite
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
