@extends('layouts.global')
@section('content')
    <div class="flex flex-wrap">
        @if ($success = Session::get('addGuest'))
            <div class="w-full mb-8 px-4">
                <div class="relative flex flex-col min-w-0 mx-auto break-words bg-white w-full w-10/12 shadow-lg rounded">
                    <div class="flex w-full mx-auto bg-notice-success">
                        <div class="w-16 bg-green">
                            <div class="p-4 py-8">
                                <svg class="my-auto h-11 w-11 text-white fill-current" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 512 512">
                                    <path
                                        d="M437.019 74.981C388.667 26.629 324.38 0 256 0S123.333 26.63 74.981 74.981 0 187.62 0 256s26.629 132.667 74.981 181.019C123.332 485.371 187.62 512 256 512s132.667-26.629 181.019-74.981C485.371 388.667 512 324.38 512 256s-26.629-132.668-74.981-181.019zM256 470.636C137.65 470.636 41.364 374.35 41.364 256S137.65 41.364 256 41.364 470.636 137.65 470.636 256 374.35 470.636 256 470.636z" />
                                    <path
                                        d="M256 235.318c-11.422 0-20.682 9.26-20.682 20.682v94.127c0 11.423 9.26 20.682 20.682 20.682 11.423 0 20.682-9.259 20.682-20.682V256c0-11.422-9.259-20.682-20.682-20.682zM270.625 147.248A20.826 20.826 0 0 0 256 141.19a20.826 20.826 0 0 0-14.625 6.058 20.824 20.824 0 0 0-6.058 14.625 20.826 20.826 0 0 0 6.058 14.625A20.83 20.83 0 0 0 256 182.556a20.826 20.826 0 0 0 14.625-6.058 20.826 20.826 0 0 0 6.058-14.625 20.839 20.839 0 0 0-6.058-14.625z" />
                                </svg>
                            </div>
                        </div>
                        <div class="w-auto text-white items-center py-4 px-4">
                            <span class="text-lg font-bold pb-4">
                                Notice
                            </span>
                            <p class="leading-tight mt-4">
                                {{ $success }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if ($success = Session::get('deleteGuest'))
            <div class="w-full mb-8 px-4">
                <div class="relative flex flex-col min-w-0 mx-auto break-words bg-white w-full w-10/12 shadow-lg rounded">
                    <div class="flex w-full mx-auto bg-notice-success">
                        <div class="w-16 bg-green">
                            <div class="p-4 py-8">
                                <svg class="my-auto h-11 w-11 text-white fill-current" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 512 512">
                                    <path
                                        d="M437.019 74.981C388.667 26.629 324.38 0 256 0S123.333 26.63 74.981 74.981 0 187.62 0 256s26.629 132.667 74.981 181.019C123.332 485.371 187.62 512 256 512s132.667-26.629 181.019-74.981C485.371 388.667 512 324.38 512 256s-26.629-132.668-74.981-181.019zM256 470.636C137.65 470.636 41.364 374.35 41.364 256S137.65 41.364 256 41.364 470.636 137.65 470.636 256 374.35 470.636 256 470.636z" />
                                    <path
                                        d="M256 235.318c-11.422 0-20.682 9.26-20.682 20.682v94.127c0 11.423 9.26 20.682 20.682 20.682 11.423 0 20.682-9.259 20.682-20.682V256c0-11.422-9.259-20.682-20.682-20.682zM270.625 147.248A20.826 20.826 0 0 0 256 141.19a20.826 20.826 0 0 0-14.625 6.058 20.824 20.824 0 0 0-6.058 14.625 20.826 20.826 0 0 0 6.058 14.625A20.83 20.83 0 0 0 256 182.556a20.826 20.826 0 0 0 14.625-6.058 20.826 20.826 0 0 0 6.058-14.625 20.839 20.839 0 0 0-6.058-14.625z" />
                                </svg>
                            </div>
                        </div>
                        <div class="w-auto text-white items-center py-4 px-4">
                            <span class="text-lg font-bold pb-4">
                                Notice
                            </span>
                            <p class="leading-tight mt-4">
                                {{ $success }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if ($message = Session::get('valEmail'))
            @if ($message2 = Session::get('valEmail2'))
            <div class="w-full mb-8 px-4">
                <div class="relative flex flex-col min-w-0 mx-auto break-words bg-white w-full w-10/12 shadow-lg rounded">
                    <div class="flex w-full mx-auto bg-notice-fail">
                        <div class="w-16 bg-green">
                            <div class="p-4 py-8">
                                <svg class="my-auto h-11 w-11 text-white fill-current" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 512 512">
                                    <path
                                        d="M437.019 74.981C388.667 26.629 324.38 0 256 0S123.333 26.63 74.981 74.981 0 187.62 0 256s26.629 132.667 74.981 181.019C123.332 485.371 187.62 512 256 512s132.667-26.629 181.019-74.981C485.371 388.667 512 324.38 512 256s-26.629-132.668-74.981-181.019zM256 470.636C137.65 470.636 41.364 374.35 41.364 256S137.65 41.364 256 41.364 470.636 137.65 470.636 256 374.35 470.636 256 470.636z" />
                                    <path
                                        d="M256 235.318c-11.422 0-20.682 9.26-20.682 20.682v94.127c0 11.423 9.26 20.682 20.682 20.682 11.423 0 20.682-9.259 20.682-20.682V256c0-11.422-9.259-20.682-20.682-20.682zM270.625 147.248A20.826 20.826 0 0 0 256 141.19a20.826 20.826 0 0 0-14.625 6.058 20.824 20.824 0 0 0-6.058 14.625 20.826 20.826 0 0 0 6.058 14.625A20.83 20.83 0 0 0 256 182.556a20.826 20.826 0 0 0 14.625-6.058 20.826 20.826 0 0 0 6.058-14.625 20.839 20.839 0 0 0-6.058-14.625z" />
                                </svg>
                            </div>
                        </div>
                        <div class="w-auto text-white items-center py-4 px-4">
                            <span class="text-lg font-bold pb-4">
                                Notice
                            </span>
                            <p class="leading-tight mt-4">
                                {{ $message }} <br>
                                {{ $message2 }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        @endif
        <div class="w-full mb-8 xl:mb-0 px-4">
            <div class="relative flex flex-col min-w-0 mx-auto break-words bg-white w-full w-10/12 mb-6 shadow-lg rounded">
                <div class="rounded-t mb-0 px-4 py-3 border-0">
                    <div class="flex flex-wrap items-center">
                        <div class="relative w-full px-4 max-w-full flex-grow flex-1">
                            <h3 class="font-semibold text-base text-gray-800">
                                Guest List
                            </h3>
                        </div>
                        <div class="relative w-full px-4 max-w-full flex-grow flex-1 text-right">
                            <a href="/guest/add"
                                class="bg-indigo-500 text-white active:bg-indigo-600 text-xs font-bold uppercase px-3 py-1 rounded outline-none focus:outline-none mr-1 mb-1 transition-custom"
                                type="button">Invite Guest</a>
                        </div>
                    </div>
                </div>
                <div class="block w-full overflow-x-auto">
                    <!-- Projects table -->
                    <table class="mt-6 items-center w-full bg-transparent border-collapse">
                        <thead>
                            <tr>
                                <th
                                    class="px-6 bg-gray-100 text-gray-600 align-middle border border-solid border-gray-200 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-no-wrap font-semibold text-left">
                                    Name
                                </th>
                                <th
                                    class="px-6 bg-gray-100 text-gray-600 align-middle border border-solid border-gray-200 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-no-wrap font-semibold text-left">
                                    Email
                                </th>
                                <th
                                    class="px-6 bg-gray-100 text-gray-600 align-middle border border-solid border-gray-200 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-no-wrap font-semibold text-left">
                                    Status
                                </th>
                                <th
                                    class="px-6 bg-gray-100 text-gray-600 align-middle border border-solid border-gray-200 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-no-wrap font-semibold text-left">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $list)
                                <tr>
                                    <th
                                        class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-no-wrap p-4 text-left">
                                        {{ $list->name }}
                                    </th>
                                    <td
                                        class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-no-wrap p-4">
                                        {{ $list->email }}
                                    </td>
                                    <td
                                        class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-no-wrap p-4">
                                        @if ($list->tokens['status'] == 'FINISH')
                                            <span
                                                class="inline-block rounded-full text-white bg-green-500 px-2 py-1 text-xs font-bold mr-3">FINISH</span>
                                        @else
                                            <span
                                                class="inline-block rounded-full text-white bg-red-500 px-2 py-1 text-xs font-bold mr-3">NOT
                                                YET</span>
                                        @endif
                                    </td>
                                    <td
                                        class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-no-wrap p-4">
                                        <a href="/guest/favorites/{{ $list->id }}"
                                            class="bg-indigo-500 text-white active:bg-indigo-600 text-xs font-bold uppercase px-3 py-1 rounded outline-none focus:outline-none mr-1 mb-1 transition-custom"
                                            type="button">Detail</a>
                                        <form onsubmit="return confirm('Are You Sure?')"
                                            action="/guest/delete/{{ $list->id }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="submit" value="Delete"
                                                class="bg-red-500 text-white active:bg-red-600 text-xs font-bold uppercase px-3 py-1 rounded outline-none focus:outline-none mr-1 mb-1">
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
