@extends('layouts.global')
@section('content')
    <div class="flex flex-wrap">
        <div class="w-full xl:mb-0 px-4">
            <div class="relative flex flex-col min-w-0 mx-auto break-words bg-white w-full w-10/12 mb-6 shadow-lg rounded">
                <div class="rounded-t mb-0 px-4 py-3 border-0">
                    <div class="flex flex-wrap items-center">
                        <div class="relative w-full px-4 max-w-full flex-grow flex-1">
                            <h3 class="font-semibold text-base text-gray-800">
                                Guest List
                            </h3>
                        </div>
                        <div class="relative w-full px-4 max-w-full flex-grow flex-1 text-right">

                        </div>
                    </div>
                </div>
                @if ($message = Session::get('valEmail'))
                    <script>
                        alert('Email is registered!');

                    </script>
                @endif
                <div class="block w-full overflow-x-auto">
                    <!-- Projects table -->
                    <table class="mt-4 items-center w-full bg-transparent border-collapse">
                        <thead>
                            <tr>
                                <th
                                    class="px-6 bg-gray-100 text-gray-600 align-middle border border-solid border-gray-200 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-no-wrap font-semibold text-left">
                                    Name
                                </th>
                                <th
                                    class="px-6 bg-gray-100 text-gray-600 align-middle border border-solid border-gray-200 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-no-wrap font-semibold text-left">
                                    Email
                                </th>
                                <th
                                    class="px-6 bg-gray-100 text-gray-600 align-middle border border-solid border-gray-200 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-no-wrap font-semibold text-left">
                                    Status
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $list)
                                <tr>
                                    <th
                                        class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-no-wrap p-4 text-left">
                                        {{ $list->name }}
                                    </th>
                                    <td
                                        class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-no-wrap p-4">
                                        {{ $list->email }}
                                    </td>
                                    <td
                                        class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-no-wrap p-4">
                                        @if ($list->tokens['status'] == 'FINISH')
                                            <span
                                                class="inline-block rounded-full text-white bg-green-500 px-2 py-1 text-xs font-bold mr-3">FINISH</span>
                                        @else
                                            <span
                                                class="inline-block rounded-full text-white bg-red-500 px-2 py-1 text-xs font-bold mr-3">NOT
                                                YET</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('designer-table')
    <div class="px-4 md:px-10 mx-auto w-full -m-24">
        <div class="flex flex-wrap">
            <div class="w-full mb-8 xl:mb-0 px-4">
                <div
                    class="relative flex flex-col min-w-0 mx-auto break-words bg-white w-full w-10/12 mb-6 shadow-lg rounded">
                    <div class="rounded-t mb-0 px-4 py-3 border-0 sm:mb-4">
                        <div class="flex flex-wrap items-center">
                            <div class="relative w-full px-4 max-w-full flex-grow flex-1">
                                <h3 class="font-semibold text-base text-gray-800">
                                    Designer Favorite List
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="block w-full overflow-x-auto">
                        <!-- Projects table -->
                        <table id="table-design" class="stripe hover table-designer">
                            <thead>
                                <tr>
                                    <th>
                                        Name Designer
                                    </th>
                                    <th>
                                        Polling Total
                                    </th>
                                    {{-- <th
                                        class="px-6 bg-gray-100 text-gray-600 align-middle border border-solid border-gray-200 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-no-wrap font-semibold text-left">
                                        Status
                                    </th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($designer as $list)
                                    <tr>
                                        <th>
                                            {{ $list->name_designer }}
                                        </th>
                                        <td>
                                            {{ $list->total }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <footer class="block py-4">
            <div class="container mx-auto px-4">
                <div class="flex flex-wrap items-center md:justify-between justify-center">
                </div>
            </div>
        </footer>
    </div>
@endsection
@push('after-style')
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">

    <style>
        /*Form fields*/
        .dataTables_wrapper select,
        .dataTables_wrapper .dataTables_filter input {
            color: #4a5568;
            /*text-gray-700*/
            padding-left: 1rem;
            /*pl-4*/
            padding-right: 1rem;
            /*pl-4*/
            padding-top: .5rem;
            /*pl-2*/
            padding-bottom: .5rem;
            /*pl-2*/
            line-height: 1.25;
            /*leading-tight*/
            border-width: 2px;
            /*border-2*/
            border-radius: .25rem;
            border-color: #edf2f7;
            /*border-gray-200*/
            background-color: #edf2f7;
            /*bg-gray-200*/
        }

        /*Row Hover*/
        table.dataTable.hover tbody tr:hover,
        table.dataTable.display tbody tr:hover {
            background-color: #ebf4ff;
            /*bg-indigo-100*/
        }

        /*Pagination Buttons*/
        .dataTables_wrapper .dataTables_paginate .paginate_button {
            font-weight: 500 !important;
            /*font-bold*/
            border-radius: .25rem;
            /*rounded*/
            border: 1px solid transparent;
            /*border border-transparent*/
        }

        /*Pagination Buttons - Current selected */
        .dataTables_wrapper .dataTables_paginate .paginate_button.current {
            color: #fff !important;
            /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06);
            /*shadow*/
            font-weight: 500 !important;
            /*font-bold*/
            border-radius: .25rem;
            /*rounded*/
            background: #667eea !important;
            /*bg-indigo-500*/
            border: 1px solid transparent;
            /*border border-transparent*/
        }

        /*Pagination Buttons - Hover */
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: #fff !important;
            /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06);
            /*shadow*/
            font-weight: 500 !important;
            /*font-bold*/
            border-radius: .25rem;
            /*rounded*/
            background: #667eea !important;
            /*bg-indigo-500*/
            border: 1px solid transparent;
            /*border border-transparent*/
        }

        /*Add padding to bottom border */
        table.dataTable.no-footer {
            border-bottom: 1px solid #e2e8f0;
            /*border-b-1 border-gray-300*/
            margin-top: 0.75em;
            margin-bottom: 0.75em;
        }

        /*Change colour of responsive icon*/
        table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before,
        table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
            background-color: #667eea !important;
            /*bg-indigo-500*/
        }

    </style>
@endpush
@push('after-script')
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

    <!--Datatables -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table-design').DataTable({
                        responsive: true
            }).columns.adjust().responsive.recalc();

            $('#table-design_length').addClass('mb-6 md:mb-0 sm:mb-0 ml-4 text-sm');
            $('#table-design_filter').addClass('mr-4');
            $('#table-design_info').addClass('ml-4');
            $('#table-design_paginate').addClass('mr-4 mb-4');
            $('#table-design > thead').addClass('bg-gray-100 font-normal');
            $('#table-design > tbody').addClass('text-left font-normal text-sm');
            $('#table-design > tbody > tr > th').addClass('font-normal');
            $('#table-design_wrapper').addClass('mt-4 md:mt-0');
            // $('#table-design_length > label').addClass('float-left');
            // $('#table-design_filter > label').addClass('ml-4 float-left mt-4 sm:mt-0');
            // $('#table-design > thead > tr > th').addClass('text-sm');
        });

    </script>
@endpush
