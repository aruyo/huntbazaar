@extends('layouts.global')
@section('content')
    <div class="flex flex-wrap mt-4">
        @if ($success = Session::get('update'))
            <div class="w-full mb-8 px-4">
                <div class="relative flex flex-col min-w-0 mx-auto break-words bg-white w-full w-10/12 shadow-lg rounded">
                    <div class="flex w-full mx-auto bg-notice-success">
                        <div class="w-16 bg-green">
                            <div class="p-4 py-8">
                                <svg class="my-auto h-11 w-11 text-white fill-current" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 512 512">
                                    <path
                                        d="M437.019 74.981C388.667 26.629 324.38 0 256 0S123.333 26.63 74.981 74.981 0 187.62 0 256s26.629 132.667 74.981 181.019C123.332 485.371 187.62 512 256 512s132.667-26.629 181.019-74.981C485.371 388.667 512 324.38 512 256s-26.629-132.668-74.981-181.019zM256 470.636C137.65 470.636 41.364 374.35 41.364 256S137.65 41.364 256 41.364 470.636 137.65 470.636 256 374.35 470.636 256 470.636z" />
                                    <path
                                        d="M256 235.318c-11.422 0-20.682 9.26-20.682 20.682v94.127c0 11.423 9.26 20.682 20.682 20.682 11.423 0 20.682-9.259 20.682-20.682V256c0-11.422-9.259-20.682-20.682-20.682zM270.625 147.248A20.826 20.826 0 0 0 256 141.19a20.826 20.826 0 0 0-14.625 6.058 20.824 20.824 0 0 0-6.058 14.625 20.826 20.826 0 0 0 6.058 14.625A20.83 20.83 0 0 0 256 182.556a20.826 20.826 0 0 0 14.625-6.058 20.826 20.826 0 0 0 6.058-14.625 20.839 20.839 0 0 0-6.058-14.625z" />
                                </svg>
                            </div>
                        </div>
                        <div class="w-auto text-white items-center py-4 px-4">
                            <span class="text-lg font-bold pb-4">
                                Notice
                            </span>
                            <p class="leading-tight mt-4">
                                {{ $success }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if ($success = Session::get('notChange'))
            <div class="w-full mb-8 px-4">
                <div class="relative flex flex-col min-w-0 mx-auto break-words bg-white w-full w-10/12 shadow-lg rounded">
                    <div class="flex w-full mx-auto bg-notice-fail">
                        <div class="w-16 bg-green">
                            <div class="p-4 py-8">
                                <svg class="my-auto h-11 w-11 text-white fill-current" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 512 512">
                                    <path
                                        d="M437.019 74.981C388.667 26.629 324.38 0 256 0S123.333 26.63 74.981 74.981 0 187.62 0 256s26.629 132.667 74.981 181.019C123.332 485.371 187.62 512 256 512s132.667-26.629 181.019-74.981C485.371 388.667 512 324.38 512 256s-26.629-132.668-74.981-181.019zM256 470.636C137.65 470.636 41.364 374.35 41.364 256S137.65 41.364 256 41.364 470.636 137.65 470.636 256 374.35 470.636 256 470.636z" />
                                    <path
                                        d="M256 235.318c-11.422 0-20.682 9.26-20.682 20.682v94.127c0 11.423 9.26 20.682 20.682 20.682 11.423 0 20.682-9.259 20.682-20.682V256c0-11.422-9.259-20.682-20.682-20.682zM270.625 147.248A20.826 20.826 0 0 0 256 141.19a20.826 20.826 0 0 0-14.625 6.058 20.824 20.824 0 0 0-6.058 14.625 20.826 20.826 0 0 0 6.058 14.625A20.83 20.83 0 0 0 256 182.556a20.826 20.826 0 0 0 14.625-6.058 20.826 20.826 0 0 0 6.058-14.625 20.839 20.839 0 0 0-6.058-14.625z" />
                                </svg>
                            </div>
                        </div>
                        <div class="w-auto text-white items-center py-4 px-4">
                            <span class="text-lg font-bold pb-4">
                                Notice
                            </span>
                            <p class="leading-tight mt-4">
                                {{ $success }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="w-full w-8/12 mb-12 xl:mb-0 px-4">
            <div class="relative flex flex-col min-w-0 break-words bg-white mb-4 shadow-lg rounded mx-auto w-10/12 h-full">
                <div class="rounded-t mb-0 px-4 py-3 border-0">
                    <div class="flex flex-wrap items-center">
                        <div class="relative w-full px-4 max-w-full flex-grow flex-1">
                            <h3 class="font-semibold mt-4 text-center text-base text-gray-800">
                                Change Due Polling
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="block w-full overflow-x-auto">
                    <!-- Projects table -->
                    <form class="mt-6 mx-6" method="POST" action="/change-time">
                        @csrf
                        @method('put')
                        @php
                        $arr = explode(" ", $data->datetime);
                        $clock = $arr[0].'T'.$arr[1];
                        @endphp
                        <label class="block uppercase text-gray-700 text-xs font-bold mb-2" for="grid-datetime">Date
                            Time (DD-MM-YYYY HH:MI)</label><input id="datetime-local" type="datetime-local"
                            class="px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full transition-custom"
                            placeholder="datetime" name="datetime" value="{{ $clock }}" />
                        <button type="submit"
                            class="w-full py-3 mb-4 mt-10 font-medium tracking-widest text-white uppercase bg-blue-800 shadow-lg focus:outline-none hover:bg-blue-900 hover:shadow-none">
                            Change
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $('#datetime-local').click(function() {
                $('#datetime-local').val('');
            })
        });
    </script>
@endpush
