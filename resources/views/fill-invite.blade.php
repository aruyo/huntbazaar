@extends('layouts.auth-layout')
@section('content')
    <main>
        <section class="absolute w-full h-full">
            <div class="absolute top-0 w-full h-full bg-blue-700 bg-register-2">
            </div>
            <div class="container mx-auto px-4 h-full">
                <div class="flex content-center items-center justify-center h-full">
                    <div class="w-full lg:w-6/12 px-4">
                        <div class="relative flex flex-col min-w-0 break-words w-full rounded-lg bg-transparent border-0">
                            <div class="rounded-t mb-0 px-6">
                                <div class="text-center mb-3">
                                    {{-- <h6 class="text-gray-600 text-sm font-bold">
                                        Welcome, Please fill in the form below <i class="fas fa-smile-wink"></i>
                                    </h6> --}}
                                    <div class="text-xl text-white font-semibold py-1" id="timer">
                                        <p>Expired On : <span><code>@{{ message }}</code></span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($errors->any())
                        <div class="w-full mb-8">
                            <div class="relative flex flex-col min-w-0 mx-auto break-words bg-white w-full shadow-lg rounded">
                                <div class="flex w-full mx-auto bg-notice-fail">
                                    <div class="w-16 bg-green">
                                        <div class="p-4 py-8">
                                            <svg class="my-auto h-11 w-11 text-white fill-current" xmlns="http://www.w3.org/2000/svg"
                                                viewBox="0 0 512 512">
                                                <path
                                                    d="M437.019 74.981C388.667 26.629 324.38 0 256 0S123.333 26.63 74.981 74.981 0 187.62 0 256s26.629 132.667 74.981 181.019C123.332 485.371 187.62 512 256 512s132.667-26.629 181.019-74.981C485.371 388.667 512 324.38 512 256s-26.629-132.668-74.981-181.019zM256 470.636C137.65 470.636 41.364 374.35 41.364 256S137.65 41.364 256 41.364 470.636 137.65 470.636 256 374.35 470.636 256 470.636z" />
                                                <path
                                                    d="M256 235.318c-11.422 0-20.682 9.26-20.682 20.682v94.127c0 11.423 9.26 20.682 20.682 20.682 11.423 0 20.682-9.259 20.682-20.682V256c0-11.422-9.259-20.682-20.682-20.682zM270.625 147.248A20.826 20.826 0 0 0 256 141.19a20.826 20.826 0 0 0-14.625 6.058 20.824 20.824 0 0 0-6.058 14.625 20.826 20.826 0 0 0 6.058 14.625A20.83 20.83 0 0 0 256 182.556a20.826 20.826 0 0 0 14.625-6.058 20.826 20.826 0 0 0 6.058-14.625 20.839 20.839 0 0 0-6.058-14.625z" />
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="w-auto text-white items-center py-4 px-4">
                                        <span class="text-lg font-bold pb-4">
                                            Notice
                                        </span>
                                        <p class="leading-tight mt-4">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300 border-0">
                            <div class="rounded-t mb-0 px-6 py-6">
                                <div class="text-center mb-3">
                                    <h6 class="text-gray-600 text-sm font-bold">
                                        Welcome, Please fill in the form below <i class="fas fa-smile-wink"></i>
                                    </h6>
                                </div>
                                <hr class="mt-6 border-b-1 border-gray-400" />
                            </div>
                            <div id="form-wrap" class="flex-auto px-4 lg:px-10 py-10 pt-0">
                                <form method="POST" action="/guest/fill">
                                    @csrf
                                    @method('put')
                                    <div class="relative w-full mb-3">
                                        <label class="block uppercase text-gray-700 text-xs font-bold mb-2"
                                            for="grid-password">Email</label><input id="email" type="email"
                                            class="px-3 py-3 placeholder-gray-400 text-gray-700 bg-gray-200 rounded text-sm shadow focus:outline-none focus:shadow-outline w-full"
                                            placeholder="Email" name="email" value="{{ old('email') }}" required readonly />
                                    </div>
                                    <div class="grid lg:grid-cols-2 md:grid-cols-2 grid-cols-1 relative w-full mb-3">
                                        <div class="lg:mr-2 md:mr-1">
                                            <label class="block uppercase text-gray-700 text-xs font-bold mb-2"
                                                for="grid-first">First Name</label><input type="text"
                                                class="px-3 py-3 placeholder-gray-400 transition-custom text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full"
                                                placeholder="First Name"
                                                name="first_name" value="{{ old('first_name') }}" required autofocus />
                                        </div>
                                        <div class="lg:ml-2 md:ml-1">
                                            <label class="block uppercase text-gray-700 text-xs font-bold mb-2"
                                                for="grid-last">Last Name</label><input type="text"
                                                class="px-3 py-3 placeholder-gray-400 text-gray-700 transition-custom bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full"
                                                placeholder="Last Name"
                                                name="last_name" value="{{ old('last_name') }}" required autofocus />
                                        </div>
                                    </div>
                                    <div class="relative w-full mb-3">
                                        <label class="block uppercase text-gray-700 text-xs font-bold mb-2"
                                            for="grid-birthday">Birth Day</label><input type="date"
                                            class="px-3 py-3 placeholder-gray-400 text-gray-700 transition-custom bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full"
                                            placeholder="birthday" name="birthday"
                                            value="{{ old('birthday') }}" />
                                    </div>
                                    <div class="relative w-full mb-3">
                                        <label class="block uppercase text-gray-700 text-xs font-bold mb-2"
                                            for="grid-choose">Choose Favorite Designer</label>
                                        <select id="select-designer"
                                            class="js-example-basic-multiple w-full px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full"
                                            name="states[]" multiple="multiple">
                                        </select>
                                    </div>
                                    <div class="text-center mt-6">
                                        <button type="submit"
                                            class="bg-blue-700 text-white active:bg-gray-700 transition-custom text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                                            type="button">
                                            Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="absolute w-full bottom-0 bg-blue-700 pb-6">
                <div class="container mx-auto px-4">
                    <hr class="mb-6 border-b-1 border-blue-800" />
                    <div class="flex flex-wrap items-center md:justify-between justify-center">
                        <div class="w-full text-center px-4">
                            <div class="text-sm text-white font-semibold py-1">
                                Copyright © 2020
                                <a href="#" class="text-white hover:text-gray-400 text-sm font-semibold py-1">APJP</a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </section>
    </main>
@endsection
@push('after-style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2 {
            width: 100% !important;
        }

    </style>
@endpush
@push('after-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script>
        $(document).ready(function() {
            $.get("/designer", function(data) {
                $('.js-example-basic-multiple').select2({
                    width: 'resolve',
                    data: data,
                });
            });

            $.get("/date", function(data) {
                console.log(data.datetime);
                var countDownDate = new Date(data.datetime).getTime();

                var x = setInterval(function() {
                    var now = new Date().getTime();
                    var distance = countDownDate - now;
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    $('#timer').html("Expired On : " + days + "Days " + hours + "h " +
                        minutes + "m " + seconds + "s ");

                    // Output the result in an element with id="timer" using vue
                    var timer = new Vue({
                        el: '#timer',
                        data: {
                            message: days + 'Days ' + hours + 'h ' + minutes + 'm ' +
                                seconds + 's '
                        }
                    })

                    // If the count down is over, write some text
                    if (distance < 0) {
                        clearInterval(x);
                        $('#timer').html('EXPIRED !');
                        $('#form-wrap').html(`<div class="relative w-full mb-3">
                                                    <div class="px-3 py-3 placeholder-gray-400 text-gray-700 bg-gray-200 rounded text-2xl font-bold shadow focus:outline-none focus:shadow-outline w-full text-center">Guest polling time period has been expire</div>
                                                </div>`);
                    }
                }, 1000);
            });

        });
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const email = urlParams.get('email')
        document.getElementById("email").value = email;

    </script>
@endpush
