<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <link rel="shortcut icon" href="./assets/img/favicon.ico" />
    <link rel="apple-touch-icon" sizes="76x76" href="./img/apple-icon.png" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css" />
    <link rel="stylesheet" href="{{ asset('vendor/@fontawesome/fontawesome-free/css/all.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <title>Hunt Bazzar</title>
    @stack('after-style')
</head>

<body class="text-gray-800 antialiased">
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root">
        <nav
            class="md:left-0 md:block md:fixed md:top-0 md:bottom-0 md:overflow-y-auto md:flex-row md:flex-no-wrap md:overflow-hidden shadow-xl bg-white flex flex-wrap items-center justify-between relative md:w-64 z-10 py-4 px-6">
            <div class="md:flex-col md:items-stretch md:min-h-full md:flex-no-wrap flex flex-wrap items-center justify-between w-full mx-auto">
                <button
                    class="cursor-pointer text-black opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
                    type="button" onclick="toggleNavbar('example-collapse-sidebar')">
                    <i class="fas fa-bars"></i></button>
                <a class="md:block text-left md:pb-2 text-gray-700 mr-0 inline-block whitespace-no-wrap text-sm uppercase font-bold p-2 px-0"
                    href="javascript:void(0)">
                    Hunt Bazzar
                </a>
                <ul class="md:hidden items-center flex flex-wrap list-none">
                    <li class="inline-block relative">
                        <a class="text-gray-600 block" href="#"
                            onclick="openDropdown(event,'user-responsive-dropdown')">
                            <div class="items-center flex">
                                <span
                                    class="icon-topbar w-12 h-12 text-sm text-white active:text-white hover:text-white focus:text-white active:bg-gray-400 hover:bg-gray-400 focus:bg-gray-400 bg-gray-200 inline-flex items-center justify-center rounded-full">
                                    <i class="fas fa-caret-down fa-2x"></i>
                                </span>
                            </div>
                        </a>
                        <div class="hidden bg-white text-base z-50 float-left py-2 list-none text-left rounded shadow-lg mt-1 min-width-2" id="user-responsive-dropdown">
                            <div href="#"
                                class="text-sm font-bold py-2 px-4 font-normal block w-full whitespace-no-wrap bg-transparent text-gray-800"><span
                                class="icon-topbar w-10 h-10 mr-2 text-sm text-white active:bg-gray-700 hover:bg-gray-700 focus:bg-gray-700 bg-gray-300 inline-flex items-center justify-center rounded-full mr-4">
                                <i
                                    class="fas fa-user"></i></span>{{ Auth::user()->name }}</div>
                                <div class="h-0 my-2 border border-solid border-gray-200"></div>
                            <a class="text-sm py-2 px-4 font-normal block w-full whitespace-no-wrap bg-transparent text-gray-800"
                                href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <span
                                class="icon-topbar w-10 h-10 mr-2 text-sm text-gray-800 active:bg-gray-700 hover:bg-gray-700 focus:bg-gray-700 bg-gray-300 inline-flex items-center justify-center rounded-full mr-4">
                                <i class="fas fa-door-open fa-lg"></i></span>Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="display-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
                <div class="md:flex md:flex-col md:items-stretch md:opacity-100 md:relative md:mt-4 md:shadow-none shadow absolute top-0 left-0 right-0 z-40 overflow-y-auto overflow-x-hidden h-auto items-center flex-1 rounded hidden m-0 margin-1"
                    id="example-collapse-sidebar">
                    <div class="md:min-w-full md:hidden block pb-4 mb-4 border-b border-solid border-gray-300">
                        <div class="flex flex-wrap">
                            <div class="w-6/12">
                                <a class="md:block text-left md:pb-2 text-gray-700 mr-0 inline-block whitespace-no-wrap text-sm uppercase font-bold p-4 px-0"
                                    href="javascript:void(0)">
                                    Hunt Bazzar
                                </a>
                            </div>
                            <div class="w-6/12 flex justify-end">
                                <button type="button"
                                    class="cursor-pointer text-black opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
                                    onclick="toggleNavbar('example-collapse-sidebar')">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <ul class="md:flex-col mt-4 md:min-w-full flex flex-col list-none">
                        <li class="items-center">
                            <a class="text-gray-800 hover:text-gray-600 text-xs uppercase py-3 font-bold block"
                                href="/dashboard"><i class="fas fa-tv opacity-75 mr-2 text-sm"></i>
                                Dashboard</a>
                        </li>
                        <li class="items-center">
                            <a class="text-gray-800 hover:text-gray-600 text-xs uppercase py-3 font-bold block"
                                href="/guest/list"><i class="fas fa-user-circle text-gray-500 mr-2 text-sm"></i>
                                Guest List</a>
                        </li>
                        <li class="items-center">
                            <a class="text-gray-800 hover:text-gray-600 text-xs uppercase py-3 font-bold block"
                                href="/setting"><i class="fas fa-cogs text-gray-500 mr-2 text-sm"></i>
                                Setting</a>
                        </li>
                        {{-- <li class="items-center">
                            <a class="text-gray-800 hover:text-gray-600 text-xs uppercase py-3 font-bold block"
                                href="#/login"><i class="fas fa-fingerprint text-gray-500 mr-2 text-sm"></i>
                                Login</a>
                        </li>
                        <li class="items-center">
                            <a class="text-gray-400 text-xs uppercase py-3 font-bold block" href="#"><i
                                    class="fas fa-clipboard-list text-gray-400 mr-2 text-sm"></i>
                                Register (soon)</a>
                        </li>
                        <li class="items-center">
                            <a class="text-gray-400 text-xs uppercase py-3 font-bold block" href="#"><i
                                    class="fas fa-tools text-gray-400 mr-2 text-sm"></i>
                                Settings (soon)</a>
                        </li> --}}
                    </ul>
                </div>
            </div>
        </nav>
        <div class="relative md:ml-64 bg-gray-100">
            <nav
                class="absolute top-0 left-0 w-full z-10 bg-transparent md:flex-row md:flex-no-wrap md:justify-start flex items-center p-4">
                <div class="w-full mx-autp items-center flex justify-between md:flex-no-wrap flex-wrap md:px-10 px-4">
                    <p class="text-white text-sm uppercase hidden lg:inline-block font-semibold" id="clock"></p>
                    <ul class="flex-col md:flex-row list-none items-center hidden md:flex">
                        <a class="text-gray-600 block" href="#" onclick="openDropdown(event,'user-dropdown')">
                            <div class="items-center flex">
                                <span
                                    class="icon-topbar w-12 h-12 text-sm text-white active:text-white hover:text-white focus:text-white active:bg-gray-400 hover:bg-gray-400 focus:bg-gray-400 bg-gray-200 inline-flex items-center justify-center rounded-full">
                                    <i class="fas fa-caret-down fa-2x"></i>
                                </span>
                            </div>
                        </a>
                        <div class="hidden bg-white text-base z-50 float-left py-2 list-none text-left rounded shadow-lg mt-1 min-width-3" id="user-dropdown">
                            <div href="#"
                                class="text-sm font-bold py-2 px-4 font-normal block w-full whitespace-no-wrap bg-transparent text-gray-800"><span
                                class="icon-topbar w-10 h-10 mr-2 text-sm text-white active:bg-gray-700 hover:bg-gray-700 focus:bg-gray-700 bg-gray-300 inline-flex items-center justify-center rounded-full mr-4">
                                <i
                                    class="fas fa-user"></i></span>{{ Auth::user()->name }}</div>
                                <div class="h-0 my-2 border border-solid border-gray-200"></div>
                            <a class="text-sm py-2 px-4 font-normal block w-full whitespace-no-wrap bg-transparent hover:bg-gray-300 text-gray-800"
                                href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <span
                                class="icon-topbar w-10 h-10 mr-2 text-sm text-gray-800 active:bg-gray-700 hover:bg-gray-700 focus:bg-gray-700 bg-gray-300 inline-flex items-center justify-center rounded-full mr-4">
                                <i class="fas fa-door-open fa-lg"></i></span>
                                 Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="display-none">
                                @csrf
                            </form>
                        </div>
                    </ul>
                </div>
            </nav>
            <!-- Header -->
            <div class="relative bg-blue-600 md:pt-32 pb-32 pt-12">
                <div class="px-4 md:px-10 mx-auto w-full">
                    <div>
                        {{-- Content --}}
                        @yield('content')
                        {{-- End Content --}}
                    </div>
                </div>
            </div>
            @yield('designer-table')
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js" charset="utf-8"></script>
    @stack('after-script')
    <script type="text/javascript">
        /* Sidebar - Side navigation menu on mobile/responsive mode */
        function toggleNavbar(collapseID) {
            document.getElementById(collapseID).classList.toggle("hidden");
            document.getElementById(collapseID).classList.toggle("bg-white");
            document.getElementById(collapseID).classList.toggle("m-2");
            document.getElementById(collapseID).classList.toggle("py-3");
            document.getElementById(collapseID).classList.toggle("px-6");
        }
        /* Function for dropdowns */
        function openDropdown(event, dropdownID) {
            let element = event.target;
            while (element.nodeName !== "A") {
                element = element.parentNode;
            }
            var popper = new Popper(element, document.getElementById(dropdownID), {
                placement: "bottom-end"
            });
            document.getElementById(dropdownID).classList.toggle("hidden");
            document.getElementById(dropdownID).classList.toggle("block");
        }

    </script>
</body>

</html>
