@extends('layouts.auth-layout')
@section('content')
    <main>
        <section class="absolute w-full h-full">
            <div class="absolute top-0 w-full h-full bg-blue-700 bg-register-2">
            </div>
            <div class="container mx-auto px-4 h-full">
                <div class="flex content-center items-center justify-center h-full">
                    <div class="w-full lg:w-6/12 px-4">
                        <div
                            class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300 border-0">
                            <div class="rounded-t mb-0 px-6 py-6">
                                <div class="text-center mb-3">
                                    <h6 class="text-gray-600 text-sm font-bold">
                                        Your Registration Code
                                    </h6>
                                </div>
                                <hr class="mt-6 border-b-1 border-gray-400" />
                            </div>
                            <div class="flex-auto px-4 lg:px-10 py-10 pt-0">
                                <div class="relative w-full">
                                    <div
                                        class="px-3 py-3 placeholder-gray-400 text-gray-700 bg-gray-200 rounded text-2xl font-bold shadow focus:outline-none focus:shadow-outline w-full text-center">
                                        {{ $code }}</div>
                                    <div class="grid justify-items-center mt-4"><button href="/guest/add"
                                            class="btn bg-blue-500 text-white mx-auto active:bg-blue-600 hover:bg-blue-600 text-xs font-bold uppercase px-3 py-1 rounded outline-none focus:outline-nonetransition-custom"
                                            type="button" data-clipboard-text="{{ $code }}">Copy to Clipboard</button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="absolute w-full bottom-0 bg-blue-700 pb-6">
                <div class="container mx-auto px-4">
                    <hr class="mb-6 border-b-1 border-blue-800" />
                    <div class="flex flex-wrap items-center md:justify-between justify-center">
                        <div class="w-full text-center px-4">
                            <div class="text-sm text-white font-semibold py-1">
                                Copyright © 2020
                                <a href="#" class="text-white hover:text-gray-400 text-sm font-semibold py-1">Neptune</a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </section>
    </main>
@endsection
@push('after-style')
@endpush
@push('after-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.6/clipboard.min.js"></script>
    <script>
        var clipboard = new ClipboardJS('.btn');

        clipboard.on('success', function(e) {
            console.info('Action:', e.action);
            console.info('Text:', e.text);
            console.info('Trigger:', e.trigger);

            e.clearSelection();
        });

        clipboard.on('error', function(e) {
            console.error('Action:', e.action);
            console.error('Trigger:', e.trigger);
        });
    </script>
@endpush
