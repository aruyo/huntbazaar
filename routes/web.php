<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\GuestTokenController;
use App\Http\Controllers\SettingController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {
    Route::get('/guest/list', [GuestController::class, 'index']);
    Route::get('/guest/add', [GuestController::class, 'create']);
    Route::post('/guest/invite', [GuestController::class, 'post']);
    Route::delete('/guest/delete/{id}', [GuestController::class, 'destroy']);
    Route::get('/guest/favorites/{id}', [GuestController::class, 'detail']);
    Route::get('/dashboard', [SettingController::class, 'dashboard'])->name('dashboard');
    Route::put('/change-time', [SettingController::class, 'changeTime']);
    Route::get('/setting', [SettingController::class, 'setting']);
    Route::post('/logout', [AdminController::class, 'logout'])->name('logout');
});

Route::get('/', [GuestTokenController::class, 'inviteForm'])->middleware('guest.verified');
Route::put('/guest/fill', [GuestTokenController::class, 'postGuest']);

Route::get('/date', [SettingController::class, 'triggerTime']);
Route::get('/designer', [SettingController::class, 'designerList']);

Route::get('/registeration/{code}', [GuestTokenController::class, 'codeRegisteration']);
